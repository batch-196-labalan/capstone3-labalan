import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
					<Card.Body id="cardP3">
						<Card.Title>
							<h2>Avail Quality Products</h2>
						</Card.Title>
							<Card.Text>
							 This shop offers affordable quality products that can't be found elsewhere. If this is something you're worried about when buying items that suits your needs, then, this shop is for you! Start browsing and enjoy purchasing the products that you love!
							
							</Card.Text>	
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
					<Card.Body id="cardP3">
						<Card.Title>
							<h2>Shop even at the comforts of your home</h2>
							</Card.Title>
							<Card.Text>
							Enjoy shopping with one click. If you're looking for products that can be delivered at your doorstep, Shop here now as we offer items that can be shipped worldwide. Our products are accessible online and are made for customers like YOU. 
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-1">
					<Card.Body id="cardP3">
						<Card.Title>
							<h2>Avail discounted prices</h2>
							</Card.Title>
							<Card.Text>
							 Want more discounted prices? Purchase now and earn more rewards! Our shop not only offers discounted prices but also additional points that can be used upon purchased! The more points earned, the more discounted prices you can have! What are you waiting for? Shop now!
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
};

















