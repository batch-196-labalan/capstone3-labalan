

import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const {productId} = useParams();

	
	const [totalAmount, setTotalAmount] = useState("");
	const [quantity, setQuantity] = useState(2);


	// const atc = (productId) => {

	// }

const order = (productId) => {

		fetch('https://ancient-cove-70021.herokuapp.com/users/order', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				totalAmount : price * quantity,
				products: [
					{
						productId: productId,
						quantity: quantity
					}
				]
			})

			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: "Successfully enrolled!",
					icon: 'success',
					text: 'Thank you for enrolling'
				});

				history("/products");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: 'Please try again later'
				});
			};
		});
	};






//
	useEffect(() => {
		fetch(`https://ancient-cove-70021.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	}, [productId]);



	return(
		<div id="pView">
		<Container className="mt-5" >
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card id="pView2">
						<Card.Body >
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" onClick={() => order(productId)}>Order</Button>

								:
								<Link className="btn btn-danger" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</div>


	)
}








