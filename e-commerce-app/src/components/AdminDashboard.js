
import {useState, useEffect} from 'react'
import {Container,Card, Button, Modal, Form} from 'react-bootstrap'
import Swal from 'sweetalert2';


export default function AdminDashboard({dashboardProp}){

	// console.log(props)
	const {name, description, price, _id} = dashboardProp;
	const [modal, setModal] = useState(false)
	const [productDetails, setProductDetails] = useState({
		name: name,
		description: description,
		price: price
	})


const [productName, setProductName] = useState(dashboardProp.name);
	const [productDescription, setProductDescription] = useState(dashboardProp.description);
	const [productPrice, setProductPrice] = useState(dashboardProp.price);
	



	
	const toggleModal = () => {
		setModal(!modal)
	}


	
	const updateProduct = (_id) => {	
		fetch(`https://ancient-cove-70021.herokuapp.com/products/updateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		}).then((result) => {
					window.location.reload()
				})

			setProductName(name);
			setProductDescription(description);
			setProductPrice(price);
		})

	}




const del = (_id) => {
			fetch(`https://ancient-cove-70021.herokuapp.com/products/archiveProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Deleted Product',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		}).then((result) => {
					window.location.reload()
				})


		})
		}

	useEffect(() => {
				
		

	},[])







	return (
	<>
	
		<Container id="adminDash1">
		<Card 
			bg="light"
			text="dark"
			className="productCourse mb-5 "
		>
		<div className="profileCardBg">
			<Card.Body id="adminDash2">
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
				{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>
				{price}
				</Card.Text>
				<Button variant="outline-light" className="me-2 btn-modal" onClick={toggleModal}>Edit Details</Button>
				<Button variant="outline-light" onClick={() => del(_id)}>Disable</Button>


			</Card.Body>
		</div>
		</Card>
		</Container>




		{modal &&(
			<div className="mt-5">
			<Modal show={modal} onHide={toggleModal} size="lg"
      			aria-labelledby="contained-modal-title-vcenter"
      			centered>
			        <Modal.Body>
			        	<Form.Group controlId="productName">
			        		<Form.Label>Product Name</Form.Label>
			        		<Form.Control
			        			type="text"
			        			placeholder="Edited Product Name"
			        			   value = {productName}
						        onChange = {e => setProductName(e.target.value)}

			        			
			        		/>
			        	</Form.Group>
			        	<Form.Group controlId="productDescription">
			        		<Form.Label>Product Description</Form.Label>
			        		<Form.Control
			        			type="text"
			        			placeholder="Edited Product Description"
			        			value={productDescription}
			        			onChange = {e => setProductDescription(e.target.value)}
			        		/>
			        	</Form.Group>
			        	<Form.Group controlId="productPrice">
			        		<Form.Label>Product Price</Form.Label>
			        		<Form.Control
			        			type="number"
			        			placeholder="Edited Product Price"
			        			value={productPrice}
			        			onChange = {e => setProductPrice(e.target.value)}
			        		/>
			        	</Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={toggleModal}>
			            Close
			          </Button>
			          <Button variant="primary" onClick={() => updateProduct(_id)}>
			            Save Changes
			          </Button>
			        </Modal.Footer>
			     </Modal>
			  </div>
			)}
		</>
	)
}





















// import {useState} from 'react'
// import {Container,Card, Button, Modal, Form} from 'react-bootstrap'



// export default function AdminDashboard(props){

// 	// console.log(props)
// 	const {name, description, price, _id} = props.productProp;
// 	const [modal, setModal] = useState(false)
// 	const [productDetails, setProductDetails] = useState({
// 		name: name,
// 		description: description,
// 		price: price
// 	})
	
// 	const toggleModal = () => {
// 		setModal(!modal)
// 	}




// 	function editDetails(e){
// 		e.preventDefault()

// 		fetch(`http://localhost:4000/products/updateProduct/${_id}`,{
// 			method: 'PUT',
// 			headers: {
// 				'Content-Type': 'application/json',
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: {
// 				name: name,
// 				description: description,
// 				price: price
// 			}
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 		})
// 	}

// 	return (
// 	<>
// 		<Container>
// 		<Card 
// 			bg="dark"
// 			text="white"
// 			className="productCourse mb-5 "
// 		>
// 		<div className="profileCardBg">
// 			<Card.Body>
// 				<Card.Title> Name:
// 					<h2>{name}</h2>
// 				</Card.Title>
// 				<Card.Subtitle>Description:</Card.Subtitle>
// 				<Card.Text>
// 				{description}
// 				</Card.Text>
// 				<Card.Subtitle>Price:</Card.Subtitle>
// 				<Card.Text>
// 				{price}
// 				</Card.Text>
// 				<Button variant="outline-light" className="me-2 btn-modal" onClick={toggleModal}>Edit Details</Button>
// 				<Button variant="outline-light">Enable</Button>


// 			</Card.Body>
// 		</div>
// 		</Card>
// 		</Container>
// 		{modal &&(
// 			<div className="mt-5">
// 			<Modal show={modal} onHide={toggleModal} size="lg"
//       			aria-labelledby="contained-modal-title-vcenter"
//       			centered>
// 			        <Modal.Body>
// 			        	<Form.Group controlId="productName">
// 			        		<Form.Label>Product Name</Form.Label>
// 			        		<Form.Control
// 			        			type="text"
// 			        			placeholder="Edited Product Name"
// 			        			value={name}
// 			        		/>
// 			        	</Form.Group>
// 			        	<Form.Group controlId="productDescription">
// 			        		<Form.Label>Product Description</Form.Label>
// 			        		<Form.Control
// 			        			type="text"
// 			        			placeholder="Edited Product Description"
// 			        			value={description}
// 			        		/>
// 			        	</Form.Group>
// 			        	<Form.Group controlId="productPrice">
// 			        		<Form.Label>Product Price</Form.Label>
// 			        		<Form.Control
// 			        			type="number"
// 			        			placeholder="Edited Product Price"
// 			        			value={price}
// 			        		/>
// 			        	</Form.Group>
// 			        </Modal.Body>
// 			        <Modal.Footer>
// 			          <Button variant="secondary" onClick={toggleModal}>
// 			            Close
// 			          </Button>
// 			          <Button variant="primary" onClick={toggleModal}>
// 			            Save Changes
// 			          </Button>
// 			        </Modal.Footer>
// 			     </Modal>
// 			  </div>
// 			)}
// 		</>
// 	)
// }



















// import {Container,Card, Button} from 'react-bootstrap'


// export default function AdminDashboard(props){

// 	console.log(props)

// 	return (
// 		<Container>
// 		<Card 
// 			bg="light"
// 			text="dark"
// 			className="productCourse mb-5 "
// 		>
// 		<div className="profileCardBg">
// 			<Card.Body>
// 				<Card.Title>
// 					<h2>name</h2>
// 				</Card.Title>
// 				<Card.Subtitle>Description:</Card.Subtitle>
				
// 				<Card.Text>
// 				price
// 				</Card.Text>
// 				<Button variant="outline-dark" className="me-2">Edit Details</Button>
// 				<Button variant="outline-dark">Enable</Button>
// 			</Card.Body>
// 		</div>
// 		</Card>
// 		</Container>
// 	)
// }
