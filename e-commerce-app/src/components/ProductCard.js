

import {Card, Button, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){

	const { name, description, price, _id} = productProp;

	return(
		<Container id="cbg">
		<Col xs={12} className="mt-4" >
		<Card className="card1 p-1 mb-3" id="cardpad">
			<Card.Body id="cardP1">
				<Card.Title className="fw-bold">{name}</Card.Title>
				<Card.Subtitle> Product Description: </Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle> Price: </Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>
		</Col>
		</Container>
			
	)
}









// import {Card, Button, Col} from 'react-bootstrap';
// import {Link} from 'react-router-dom';

// export default function ProductCard(props){

// 	const {breakpoint, productProp} = props
// 	const { name, description, price, _id} = props.productProp;

// 	return(

// 		<Col xs={12} md={breakpoint} className="mt-4">
// 		<Card className="card1 p-3 mb-3">
// 			<Card.Body>
// 				<Card.Title className="fw-bold">{name}</Card.Title>
// 				<Card.Subtitle> Product Description: </Card.Subtitle>
// 				<Card.Text>
// 					{description}
// 				</Card.Text>
// 				<Card.Subtitle> Price: </Card.Subtitle>
// 				<Card.Text>{price}</Card.Text>
// 				<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
// 			</Card.Body>
// 		</Card>
// 		</Col>
			
// 	)
// }
















// export default function ProductCard({productProp}){
// 	const {_id, name, description, price} = productProp

// 	return(
// 			<Card className="cardHighlight p-3 mb-3">
// 				<Card.Body>
// 					<Card.Title className="fw-bold">
// 						{name}
// 					</Card.Title>
// 					<Card.Subtitle>Product Description</Card.Subtitle>
// 					<Card.Text>
// 						{description}
// 					</Card.Text>
// 					<Card.Subtitle>Price</Card.Subtitle>
// 					<Card.Text>
// 						{price}
// 					</Card.Text>
// 					<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
// 				</Card.Body>
// 			</Card>
// 	)
// }

