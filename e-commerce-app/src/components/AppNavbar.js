// import {Navbar, Container, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, useContext} from 'react';
import UserContext from '../UserContext';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar(){

  const {user} = useContext(UserContext);

	return(

 <Navbar bg="light" expand="lg">
      <Container fluid>
        <Nav.Link as={Link} to="/"id="AppNavbarT">HP</Nav.Link>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
            id="navId"
          >
          
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>

            

          {
            (user.id !== null) ? 
            <>
            <Nav.Link as={Link} to="/profile">
              Profile
            </Nav.Link>

             <Nav.Link as={Link} to="/logout">
              Logout
            </Nav.Link>
            
           </>

            :
            <>
            <Nav.Link as={Link} to="/login" >
              Login
            </Nav.Link>
            <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </>

          }
          
          </Nav>
          <Form className="d-flex">
            
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>

  )

};



