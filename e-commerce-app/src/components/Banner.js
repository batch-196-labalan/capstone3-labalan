import {Row, Col, Button} from 'react-bootstrap';




export default function Banner(){
	return(

		<Row>
			<Col className = "p-5">
				<h1> Harry Potter Collectibles</h1>
				<p id="p1"> Shop wherever you go</p>
				<Button variant="info" href ="http://localhost:3000/products">Shop Now</Button>
			</Col>
		</Row>
	)
};