import ProductCard from '../components/ProductCard'
import {useState, useEffect} from 'react'


import {Row} from 'react-bootstrap';

export default function Products(){
	
	const [products, setProducts] = useState([]);
	const columnsPerRow = 3;

	useEffect(() => {
		fetch("https://ancient-cove-70021.herokuapp.com/products/active")
		.then(res => res.json())
		.then(data => {
			
			const pProducts = (data.map(product =>{
				return(
					<ProductCard key={product._id} productProp = {product}/>
				)
			}));
			setProducts(pProducts);
		})

	}, [products])

	return(

	<>
		<h1 id="productT">Available Products:</h1>
		<Row md={columnsPerRow}>
		{products}
		 </Row>
	</>
	)
}












// import ProductCard from '../components/ProductCard'
// import {useState, useEffect} from 'react'


// import {Row} from 'react-bootstrap';

// export default function Products(){
	
// 	const [products, setProducts] = useState([]);
// 	const columnsPerRow = 2;

// 	useEffect(() => {
// 		fetch("http://localhost:4000/products/active")
// 		.then(res => res.json())
// 		.then(data => {
			
// 			setProducts(data.map(product =>{
// 				return(
// 					<ProductCard key={product._id} productProp = {product}/>
// 				)
// 			}));

// 		})

// 	}, [])

// 	return(

// 	<>
// 		<h1 id="productT">Available Products:</h1>
// 		<Row md={columnsPerRow}>
// 		{products}
// 		 </Row>
// 	</>
// 	)
// }
