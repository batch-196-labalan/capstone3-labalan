


import {useState, useContext, useEffect} from 'react'
import {Container, Table, Button} from 'react-bootstrap'
import {Modal, Form} from 'react-bootstrap'
import UserContext from '../UserContext';
import AdminDashboard from '../components/AdminDashboard';

import AdminProduct from '../components/AdminProduct';



export default function Profile() {

	const [products, setProducts] = useState([]);
	const [userDetails, setUserDetails] = useState({
		firstName: null,
		lastName: null,
		email: null,
		mobileNo: null,
		isAdmin: null

	})

	useEffect(()=>{
    fetch('https://ancient-cove-70021.herokuapp.com/users/getUserDetails',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data=> {
      console.log(data);
      setUserDetails({
      	firstName: data.firstName,
      	lastName: data.lastName,
      	email: data.email,
      	mobileNo: data.mobileNo,
      	isAdmin: data.isAdmin
      })
  	})
	},[])

	useEffect(()=>{
		fetch('https://ancient-cove-70021.herokuapp.com/products/active',{
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return (
					<AdminDashboard key={product._id} dashboardProp={product}/>
				)
			}))
		})
	},[])




//A

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);




	const {user} = useContext(UserContext);
	const [name, setName] = useState('')
	const [desc, setDesc] = useState('')
	const [price, setPrice] = useState('')
	const [isActive, setIsActive] = useState(false)



useEffect(() => {

		if(name !== '' && desc !== '' && price > 0) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, desc, price])




	return(
		<div id="profileT" >
		{
		 (userDetails.isAdmin === false) ?

		 <>
		 <h1 className="text-center text-light">User Dashboard</h1>
		 </>
		 :
		 <>
		 	<h1 className="text-center text-light">Admin Dashboard</h1>
		 	



<>
<AdminProduct/>
</>



	{products}

		 </>

		 }
		<Container className="pt-5">
		<div id="border">
		<Table striped bordered hover variant="dark" >
		      <thead >
		        <tr>
		          <th colSpan={2} className="text-center">Profile</th>
		          
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td>First Name</td>
		          <td>{userDetails.firstName}</td>
		          
		        </tr>
		        <tr>
		          <td>Last Name</td>
		          <td>{userDetails.lastName}</td>
		        </tr>
		        
		        <tr>
		          <td>Email</td>
		          <td>{userDetails.email}</td>
		        </tr>
		        
		        <tr>
		          <td>Mobile No.</td>
		          <td>{userDetails.mobileNo}</td>
		        </tr>
		        
		      </tbody>
		    </Table>
		 </div>   
		 </Container>


	  

		 
		</div>
	)
}








// import {useState, useEffect} from 'react'
// import {Container, Table, Button} from 'react-bootstrap'
// import {Modal} from 'react-bootstrap'
// import AdminDashboard from '../components/AdminDashboard';

// export default function Profile() {

// 	const [products, setProducts] = useState([]);
// 	const [userDetails, setUserDetails] = useState({
// 		firstName: null,
// 		lastName: null,
// 		email: null,
// 		mobileNo: null,
// 		isAdmin: null

// 	})

// 	useEffect(()=>{
//     fetch('http://localhost:4000/users/getUserDetails',{
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('token')}`
//       }
//     })
//     .then(res => res.json())
//     .then(data=> {
//       console.log(data);
//       setUserDetails({
//       	firstName: data.firstName,
//       	lastName: data.lastName,
//       	email: data.email,
//       	mobileNo: data.mobileNo,
//       	isAdmin: data.isAdmin
//       })
//   	})
// 	},[])

// 	useEffect(()=>{
// 		fetch('http://localhost:4000/users/getAllOrders',{
// 			method: 'GET',
// 			headers: {
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			}
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			// console.log(data)
// 			setProducts(data.map(product => {
// 				return (
// 					<AdminDashboard key={product.id} productProp={product}/>
// 				)
// 			}))
// 		})
// 	},[])




// 	const [show, setShow] = useState(false);

// 	const handleClose = () => setShow(false);
// 	const handleShow = () => setShow(true);







// 	return(
// 		<div id="profileT" >
// 		{
// 		 (userDetails.isAdmin === false) ?

// 		 <>
// 		 <h1 className="text-center text-light">User Dashboard</h1>
// 		 </>
// 		 :
// 		 <>
// 		 	<h1 className="text-center text-light">Admin Dashboard</h1>
// 		 	{products}
// 		 </>
// 		 }
// 		<Container className="pt-5">
// 		<div id="border">
// 		<Table striped bordered hover variant="light">
// 		      <thead>
// 		        <tr>
// 		          <th colSpan={2} className="text-center">Profile</th>
		          
// 		        </tr>
// 		      </thead>
// 		      <tbody>
// 		        <tr>
// 		          <td>First Name</td>
// 		          <td>{userDetails.firstName}</td>
		          
// 		        </tr>
// 		        <tr>
// 		          <td>Last Name</td>
// 		          <td>{userDetails.lastName}</td>
// 		        </tr>
		        
// 		        <tr>
// 		          <td>Email</td>
// 		          <td>{userDetails.email}</td>
// 		        </tr>
		        
// 		        <tr>
// 		          <td>Mobile No.</td>
// 		          <td>{userDetails.mobileNo}</td>
// 		        </tr>
		        
// 		      </tbody>
// 		    </Table>
// 		 </div>   
// 		 </Container>




// 	  <div>
// 		 <Button id="profbtn" variant="primary" onClick={handleShow}>
// 				Add New Product
// 			</Button>

// 			<Modal
// 				show={show}
// 				onHide={handleClose}
// 				backdrop="static"
// 				keyboard={false}
// 			>
// 				<Modal.Header closeButton>
// 					<Modal.Title>Add Product</Modal.Title>
// 				</Modal.Header>
// 				<Modal.Body>
// 				SAMPLES
// 				</Modal.Body>
// 				<Modal.Footer>
// 					<Button variant="secondary" onClick={handleClose}>
// 						Close
// 					</Button>
// 					<Button variant="primary">Understood</Button>
// 				</Modal.Footer>
// 			</Modal>
// 	</div>


		 
// 		</div>
// 	)
// }

