import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';
import AppNavbar from './components/AppNavbar';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import Home from './pages/Home';
import Products from './pages/Products';
import Login from './pages/Login';
import Register from './pages/Register';
import Error from './pages/Error';
import Logout from './pages/Logout';
import ProductView from './components/ProductView';

import Profile from './pages/Profile';



import './App.css';



function App() {

const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    const unsetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
        fetch("https://ancient-cove-70021.herokuapp.com/users/getUserDetails", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (res => res.json())
        .then(data => {
            console.log(data);

            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            }
            else {
                // set back the initial state of user
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    }, []);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}> 
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>}/>
              <Route exact path="/products" element={<Products/>}/>
              <Route exact path="/productView/:productId" element={<ProductView/>}/>
              <Route exact path="/register" element={<Register/>}/>
              <Route exact path="/login" element={<Login/>}/>
              <Route exact path="/logout" element={<Logout/>}/>
              <Route exact path="*" element={<Error/>}/>
              <Route exact path="/profile" element={<Profile/>}/>
              
            </Routes>     
          </Container>
        </Router> 
      </UserProvider>
    </>

  );
}

export default App;
